module.exports = {
  purge: [],
  theme: {
    extend: {
      colors: {
        orange: {
          default: "#F15D25",
          light: "#F57C22",
          dark: "#EE3825"
        }
      }
    },
    gradients: theme => ({
      'gradient-orange': ['to right', theme('colors.orange.dark'), theme('colors.orange.light')],
      'gradient-orange-secondary': ['to right', theme('colors.orange.light'), theme('colors.orange.dark')]
    }),
  },
  variants: {
    gradients: ['responsive', 'hover'],
  },
  plugins: [
    require('tailwindcss-plugins/gradients'),
  ],
}
