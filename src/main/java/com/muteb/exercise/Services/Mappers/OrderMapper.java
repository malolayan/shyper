package com.muteb.exercise.Services.Mappers;

import com.muteb.exercise.Models.Order;
import com.muteb.exercise.Services.dtos.OrderDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {CityMapper.class})
public interface OrderMapper extends EntityMapper<OrderDTO, Order> {

    OrderDTO toDto(Order order);
    Order toEntity(OrderDTO orderDTO);

    default Order fromId(Long id) {
        if (id == null) {
            return null;
        }

        Order order = new Order();
        order.setId(id);
        return order;
    }
}
