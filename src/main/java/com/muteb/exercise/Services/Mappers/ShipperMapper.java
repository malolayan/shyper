package com.muteb.exercise.Services.Mappers;

import com.muteb.exercise.Models.Shipper;
import com.muteb.exercise.Services.dtos.ShippersDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ShipperMapper extends EntityMapper<ShippersDTO, Shipper> {

    ShippersDTO toDto(Shipper shipper);
    Shipper toEntity(ShippersDTO shippersDTO);

    default Shipper fromId(Long id) {
        if (id == null) {
            return null;
        }

        Shipper shipper = new Shipper();
        shipper.setId(id);
        return shipper;
    }
}
