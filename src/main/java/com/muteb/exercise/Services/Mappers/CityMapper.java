package com.muteb.exercise.Services.Mappers;

import com.muteb.exercise.Models.City;
import com.muteb.exercise.Services.dtos.CityDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CityMapper extends EntityMapper<CityDTO, City> {
    CityDTO toDto(City city);
    City toEntity(CityDTO cityDTO);

    default City fromId(Long id) {
        if (id == null) {
            return null;
        }
        City city = new City();
        city.setId(id);
        return city;
    }
}
