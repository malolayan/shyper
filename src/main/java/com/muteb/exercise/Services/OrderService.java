package com.muteb.exercise.Services;

import com.muteb.exercise.Models.Order;
import com.muteb.exercise.Repositories.OrderRepository;
import com.muteb.exercise.Services.Mappers.OrderMapper;
import com.muteb.exercise.Services.dtos.OrderDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class OrderService {

    private Logger log = LoggerFactory.getLogger(OrderService.class);
    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;

    public OrderService(OrderRepository orderRepository, OrderMapper orderMapper) {
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
    }

    public OrderDTO saveNewOrder(OrderDTO orderDTO) {
        log.debug("save a new order {}", orderDTO);
        Order order = orderMapper.toEntity(orderDTO);
        order = orderRepository.save(order);
        return orderMapper.toDto(order);
    }
}
