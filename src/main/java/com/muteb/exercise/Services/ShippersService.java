package com.muteb.exercise.Services;

import com.muteb.exercise.Models.Shipper;
import com.muteb.exercise.Models.ShipperService;
import com.muteb.exercise.Repositories.ShipperServiceRepository;
import com.muteb.exercise.Repositories.ShippersRepository;
import com.muteb.exercise.Services.Mappers.ShipperMapper;
import com.muteb.exercise.Services.dtos.ShippersDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class ShippersService {

    private Logger log = LoggerFactory.getLogger(ShippersService.class);
    private final ShippersRepository shippersRepository;
    private final ShipperMapper shipperMapper;
    private final ShipperServiceRepository shipperServiceRepository;

    public ShippersService(ShippersRepository shippersRepository, ShipperMapper shipperMapper,
                           ShipperServiceRepository shipperServiceRepository) {
        this.shippersRepository = shippersRepository;
        this.shipperMapper = shipperMapper;
        this.shipperServiceRepository = shipperServiceRepository;
    }

    public List<ShippersDTO> getAllShippers() {
        List<Shipper> shippers = shippersRepository.findAll();
        List<ShippersDTO> shippersDTOS = shipperMapper.toDto(shippers);

        shippersDTOS.stream().distinct().forEach(shipper -> {
            Set<ShipperService> getServices = shipperServiceRepository.getAllByShipperId(shipper.getId());
            shipper.setShipperServiceList(getServices);
        });
        return shippersDTOS;
    }
}
