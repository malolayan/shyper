package com.muteb.exercise.Services.dtos;

import com.muteb.exercise.Models.ShipperService;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ShippersDTO implements Serializable {

    private Long id;
    private String shipperName;
    private String logoUrl;

    private Set<ShipperService> shipperServiceList = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShipperName() {
        return shipperName;
    }

    public void setShipperName(String shipperName) {
        this.shipperName = shipperName;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Set<ShipperService> getShipperServiceList() {
        return shipperServiceList;
    }

    public void setShipperServiceList(Set<ShipperService> shipperServiceList) {
        this.shipperServiceList = shipperServiceList;
    }
}
