package com.muteb.exercise.Services.dtos;

import com.muteb.exercise.Models.City;
import com.muteb.exercise.Models.ShipperService;
import com.muteb.exercise.Models.enums.CostOfShippingOnEnum;
import com.muteb.exercise.Models.enums.PaymentMethodEnum;

import java.io.Serializable;

public class OrderDTO implements Serializable {

    private Long id;
    private CityDTO cityFrom;
    private CityDTO cityTo;
    private PaymentMethodEnum paymentMethod;
    private CostOfShippingOnEnum costOfShippingOn;
    private ShipperService shipperService;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CityDTO getCityFrom() {
        return cityFrom;
    }

    public void setCityFrom(CityDTO cityFrom) {
        this.cityFrom = cityFrom;
    }

    public CityDTO getCityTo() {
        return cityTo;
    }

    public void setCityTo(CityDTO cityTo) {
        this.cityTo = cityTo;
    }

    public PaymentMethodEnum getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethodEnum paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public CostOfShippingOnEnum getCostOfShippingOn() {
        return costOfShippingOn;
    }

    public void setCostOfShippingOn(CostOfShippingOnEnum costOfShippingOn) {
        this.costOfShippingOn = costOfShippingOn;
    }

    public ShipperService getShipperService() {
        return shipperService;
    }

    public void setShipperService(ShipperService shipperService) {
        this.shipperService = shipperService;
    }
}
