package com.muteb.exercise.Services.dtos;

import com.muteb.exercise.Models.City;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class CityDTO implements Serializable {

    private Long id;
    private String cityName;
    private boolean canSendFrom;
    private Set<City> citiesCanSendTo;

    public CityDTO(long id) {
        this.id = id;
    }

    public CityDTO() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public boolean isCanSendFrom() {
        return canSendFrom;
    }

    public void setCanSendFrom(boolean canSendFrom) {
        this.canSendFrom = canSendFrom;
    }

    public Set<City> getCitiesCanSendTo() {
        return citiesCanSendTo;
    }

    public void setCitiesCanSendTo(Set<City> citiesCanSendTo) {
        this.citiesCanSendTo = citiesCanSendTo;
    }
}
