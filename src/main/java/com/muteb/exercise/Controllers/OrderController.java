package com.muteb.exercise.Controllers;

import com.muteb.exercise.Controllers.errors.BadRequestException;
import com.muteb.exercise.Models.Order;
import com.muteb.exercise.Repositories.CityRepository;
import com.muteb.exercise.Repositories.OrderRepository;
import com.muteb.exercise.Services.Mappers.CityMapper;
import com.muteb.exercise.Services.OrderService;
import com.muteb.exercise.Services.dtos.OrderDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class OrderController {
    private Logger log = LoggerFactory.getLogger(OrderController.class);
    private final OrderService orderService;
    private final OrderRepository orderRepository;
    private final CityMapper cityMapper;
    private final CityRepository cityRepository;

    public OrderController(OrderService orderService, OrderRepository orderRepository, CityMapper cityMapper, CityRepository cityRepository) {
        this.orderService = orderService;
        this.orderRepository = orderRepository;
        this.cityMapper = cityMapper;
        this.cityRepository = cityRepository;
    }

    @PostMapping("/order")
    public ResponseEntity<OrderDTO> saveNewOrder(@RequestBody OrderDTO order) throws URISyntaxException {
        log.debug("Post request to save a new Order: {}", order);
        if (!validateRequest(order)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
//            throw new BadRequestException();
        }

        OrderDTO savedOrder = orderService.saveNewOrder(order);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedOrder);
    }

    @GetMapping("/order/{id}")
    public ResponseEntity<Order> getOrderById(@PathVariable Long id) {
        Optional<Order> order = orderRepository.findById(id);
        return ResponseEntity.ok().body(order.orElse(null));
    }

    private boolean validateRequest(OrderDTO order) {
        return order.getId() == null
                && order.getCityFrom().getId() != null
                && order.getCityTo().getId() != null
                && order.getCostOfShippingOn() != null
                && order.getPaymentMethod() != null
                && order.getShipperService().getId() != null
                && order.getCityFrom().isCanSendFrom()
                && order.getCityFrom().getCitiesCanSendTo().contains(cityMapper.toEntity(order.getCityTo()));
    }
}
