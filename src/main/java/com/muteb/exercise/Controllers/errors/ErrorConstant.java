package com.muteb.exercise.Controllers.errors;

import java.net.URI;

public class ErrorConstant {

    public static final String PROBLEM_BASE_URL = "http://localhost:8080/error";
    public static final URI BAD_REQUEST = URI.create(PROBLEM_BASE_URL + "/bad-request");
}
