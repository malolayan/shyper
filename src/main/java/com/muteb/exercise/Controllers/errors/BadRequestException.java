package com.muteb.exercise.Controllers.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class BadRequestException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    public BadRequestException() {
        super(ErrorConstant.BAD_REQUEST, "badRequest", Status.BAD_REQUEST);
    }

}
