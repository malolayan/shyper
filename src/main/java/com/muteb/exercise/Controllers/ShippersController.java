package com.muteb.exercise.Controllers;

import com.muteb.exercise.Services.ShippersService;
import com.muteb.exercise.Services.dtos.ShippersDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class ShippersController {

    private Logger log = LoggerFactory.getLogger(ShippersController.class);
    private final ShippersService shippersService;

    public ShippersController(ShippersService shippersService) {
        this.shippersService = shippersService;
    }

    @GetMapping("/shippers")
    public ResponseEntity<List<ShippersDTO>> getAllShippers() {
        log.debug("Request to get all shippers all");
        List<ShippersDTO> shippersDTOS = shippersService.getAllShippers();
        return ResponseEntity.ok().body(shippersDTOS);
    }
}
