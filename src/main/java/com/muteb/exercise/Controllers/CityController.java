package com.muteb.exercise.Controllers;

import com.muteb.exercise.Models.City;
import com.muteb.exercise.Services.CityService;
import com.muteb.exercise.Services.Mappers.CityMapper;
import com.muteb.exercise.Services.dtos.CityDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CityController {

    private final Logger log = LoggerFactory.getLogger(CityController.class);
    private final CityService cityService;
    private final CityMapper cityMapper;

    public CityController(CityService cityService, CityMapper cityMapper) {
        this.cityService = cityService;
        this.cityMapper = cityMapper;
    }

    @GetMapping("/cities")
    public ResponseEntity<List<CityDTO>> getAllCities() {
        log.debug("Request to get all cities");
        List<City> cities = cityService.getAllCities();
        List<CityDTO> cityDTOS = cityMapper.toDto(cities);
        for(CityDTO cityDTO: cityDTOS) {
            cities.stream().filter(city -> city.getId().equals(cityDTO.getId())).findFirst().ifPresent(
                    tempCity -> cityDTO.setCitiesCanSendTo(tempCity.getCitiesCanSendTo()));
        }
        return ResponseEntity.ok().body(cityDTOS);
    }
}
