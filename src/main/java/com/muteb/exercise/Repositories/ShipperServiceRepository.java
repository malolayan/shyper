package com.muteb.exercise.Repositories;

import com.muteb.exercise.Models.ShipperService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Set;

@Repository
public interface ShipperServiceRepository extends JpaRepository<ShipperService, Long> {

    Set<ShipperService> getAllByShipperId(Long shipperId);
}
