package com.muteb.exercise.Repositories;

import com.muteb.exercise.Models.Shipper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShippersRepository extends JpaRepository<Shipper, Long> {
}
