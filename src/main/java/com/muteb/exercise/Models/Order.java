package com.muteb.exercise.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.muteb.exercise.Models.enums.CostOfShippingOnEnum;
import com.muteb.exercise.Models.enums.PaymentMethodEnum;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "shipping_order")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "order_sequence")
    @SequenceGenerator(name = "order_sequence", sequenceName = "order_sequence", allocationSize = 1)
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties("")
    @JoinColumn(name="city_from_id", referencedColumnName = "id")
    private City cityFrom;

    @ManyToOne
    @JsonIgnoreProperties("")
    @JoinColumn(name="city_to_id", referencedColumnName = "id")
    private City cityTo;

    @Column(name = "payment_method")
    @Enumerated(EnumType.STRING)
    private PaymentMethodEnum paymentMethod;

    @Enumerated(EnumType.STRING)
    @Column(name = "cost_of_shipping_on")
    private CostOfShippingOnEnum costOfShippingOn;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name="shipment_service_id", referencedColumnName = "id")
    @NotNull
    private ShipperService shipperService;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PaymentMethodEnum getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethodEnum paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public CostOfShippingOnEnum getCostOfShippingOn() {
        return costOfShippingOn;
    }

    public void setCostOfShippingOn(CostOfShippingOnEnum costOfShippingOn) {
        this.costOfShippingOn = costOfShippingOn;
    }

    public City getCityFrom() {
        return cityFrom;
    }

    public void setCityFrom(City cityFrom) {
        this.cityFrom = cityFrom;
    }

    public City getCityTo() {
        return cityTo;
    }

    public void setCityTo(City cityTo) {
        this.cityTo = cityTo;
    }

    public ShipperService getShipperService() {
        return shipperService;
    }

    public void setShipperService(ShipperService shipperService) {
        this.shipperService = shipperService;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id.equals(order.id) &&
                cityFrom.equals(order.cityFrom) &&
                cityTo.equals(order.cityTo) &&
                paymentMethod == order.paymentMethod &&
                costOfShippingOn == order.costOfShippingOn &&
                shipperService.equals(order.shipperService);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cityFrom, cityTo, paymentMethod, costOfShippingOn, shipperService);
    }
}
