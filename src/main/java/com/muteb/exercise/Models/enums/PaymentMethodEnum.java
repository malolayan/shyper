package com.muteb.exercise.Models.enums;

public enum PaymentMethodEnum {
    CASH_ON_DELIVERY, PREPAID
}
