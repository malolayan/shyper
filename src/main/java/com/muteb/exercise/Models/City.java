package com.muteb.exercise.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "city")
public class City implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name= "name")
    private String cityName;

    @Column(name = "can_send_from")
    private boolean canSendFrom;

    @ManyToMany
    @JoinTable(name="city_relations_to_send_to",
            joinColumns=@JoinColumn(name="city_from_id"),
            inverseJoinColumns=@JoinColumn(name="city_to_id")
    )
    @JsonIgnore
    private Set<City> citiesCanSendTo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public boolean isCanSendFrom() {
        return canSendFrom;
    }

    public void setCanSendFrom(boolean canSendFrom) {
        this.canSendFrom = canSendFrom;
    }

    public Set<City> getCitiesCanSendTo() {
        return citiesCanSendTo;
    }

    public void setCitiesCanSendTo(Set<City> citiesCanSendTo) {
        this.citiesCanSendTo = citiesCanSendTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return canSendFrom == city.canSendFrom &&
                id.equals(city.id) &&
                cityName.equals(city.cityName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cityName, canSendFrom);
    }
}
