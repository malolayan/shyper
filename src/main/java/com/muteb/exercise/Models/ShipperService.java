package com.muteb.exercise.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "shipping_service")
public class ShipperService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "name")
    private String serviceName;

    @Column(name = "shipping_days")
    private String shippingDays;

    @Column(name = "base_price")
    private String basePrice;

    @Column(name = "details")
    private String details;

    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    @NotNull
    @JsonIgnore
    private Shipper shipper;

    public ShipperService(long id) {
        this.id = id;
    }

    public ShipperService() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getShippingDays() {
        return shippingDays;
    }

    public void setShippingDays(String shippingDays) {
        this.shippingDays = shippingDays;
    }

    public String getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(String basePrice) {
        this.basePrice = basePrice;
    }

    public Shipper getShipper() {
        return shipper;
    }

    public void setShipper(Shipper shipper) {
        this.shipper = shipper;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShipperService that = (ShipperService) o;
        return id.equals(that.id) &&
                serviceName.equals(that.serviceName) &&
                shippingDays.equals(that.shippingDays) &&
                basePrice.equals(that.basePrice) &&
                shipper.equals(that.shipper);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, serviceName, shippingDays, basePrice, shipper);
    }
}
