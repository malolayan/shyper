import {IShipperServiceModel} from "./Shipper-service.model";

export interface IShipperModel {
  id?: number;
  shipperName?: string;
  logoUrl?: string;
  shipperServiceList?: IShipperServiceModel[];

}

export class Shipper implements IShipperModel {

  constructor(
    public id?: number,
    public shipperName?: string,
    public logoUrl?: string,
    public shipperServiceList?: IShipperServiceModel[]
  ) {

  }

}
