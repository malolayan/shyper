import {ICityModel} from "./city.model";
import {IShipperServiceModel} from "./Shipper-service.model";

export const enum paymentMethodEnum {
  CASH_ON_DELIVERY = "CASH_ON_DELIVERY",
  PREPAID = "PREPAID"
}

export const enum costOfShippingOnEnum {
  STORE = "STORE",
  RECIPIENT = "RECIPIENT"
}

export interface IOrderModel {
  id?: string;
  cityFrom?: ICityModel;
  cityTo?: ICityModel;
  paymentMethod?: paymentMethodEnum
  costOfShippingOn?: costOfShippingOnEnum
  shipperService?: IShipperServiceModel
}

export class OrderModel {
  constructor(
    public id?: string,
    public cityFrom?: ICityModel,
    public cityTo?: ICityModel,
    public paymentMethod?: paymentMethodEnum,
    public costOfShippingOn?: costOfShippingOnEnum,
    public shipperService?: IShipperServiceModel
  ) {
  }
}
