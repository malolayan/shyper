export interface ICityModel {
  id?: number;
  cityName?: string;
  canSendFrom?:boolean;
  citiesCanSendTo?:ICityModel[]
}

export class CityModel implements ICityModel {
  constructor(
    public id?: number,
    public cityName?: string,
    public canSendFrom?: boolean,
    public citiesCanSendTo?:ICityModel[]
  ) {
    this.canSendFrom = this.canSendFrom || false;
  }
}
