import {IShipperModel} from "./shipper.model";

export interface IShipperServiceModel {
  id?: number;
  serviceName?: string;
  shippingDays?: string;
  basePrice?: string;
  details?: string;
  shipper?: IShipperModel;showDetails?: boolean;
}

export class ShipperServiceModel {
  constructor(
    public id?: number,
    public serviceName?: string,
    public shippingDays?: string,
    public basePrice?: string,
    public details?: string,
    public shipper?: IShipperModel,
    public showDetails?: boolean
  ) {
    showDetails = false;
  }
}
