import { Injectable } from '@angular/core';
import {BASE_RUL} from "../app.constants";
import {HttpClient, HttpResponse} from "@angular/common/http";
import {IShipperModel} from "../models/shipper.model";
import {Observable} from "rxjs";

type EntityArrayResponseType = HttpResponse<IShipperModel[]>

@Injectable({
  providedIn: 'root'
})
export class ShipperService {
  public shippersUrl = BASE_RUL + 'api/shippers';
  constructor(protected http: HttpClient) { }

  getAll(): Observable<EntityArrayResponseType> {
    return this.http.get<IShipperModel[]>(this.shippersUrl, {observe: 'response'});
  }
}
