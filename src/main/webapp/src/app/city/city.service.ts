import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {ICityModel} from "../models/city.model";
import {BASE_RUL} from "../app.constants";

type EntityArrayResponseType = HttpResponse<ICityModel[]>

@Injectable({
  providedIn: 'root'
})
export class CityService {
  public cityResource = BASE_RUL + 'api/cities';
  constructor(protected http: HttpClient) { }

  getAllCity(): Observable<EntityArrayResponseType> {
    return this.http.get<ICityModel[]>(this.cityResource, {observe: 'response'});
  }
}
