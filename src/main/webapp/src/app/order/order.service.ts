import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from "@angular/common/http";
import {IOrderModel} from "../models/order.model";
import {BASE_RUL} from "../app.constants";
import {Observable} from "rxjs";

type EntityArrayResponseType = HttpResponse<IOrderModel[]>
type EntityResponseType = HttpResponse<IOrderModel>;
@Injectable({
  providedIn: 'root'
})
export class OrderService {
  public orderResource = BASE_RUL + 'api/order';
  constructor(protected http: HttpClient) { }

  submitOrder(order: IOrderModel): Observable<EntityResponseType> {
    return this.http.post<IOrderModel>(this.orderResource, order, { observe: 'response' });
  }
}
