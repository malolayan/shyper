import {Component, OnInit} from '@angular/core';
import {CityService} from '../city';
import {ICityModel} from '../models/city.model';
import {costOfShippingOnEnum, IOrderModel, paymentMethodEnum} from '../models/order.model';
import {ShipperService} from "../shipper/shipper.service";
import {IShipperModel} from "../models/shipper.model";
import { faShippingFast } from '@fortawesome/free-solid-svg-icons';
import { faAngleDown } from '@fortawesome/free-solid-svg-icons';
import { faAngleUp } from '@fortawesome/free-solid-svg-icons';
import {IShipperServiceModel} from "../models/Shipper-service.model";
import {RIYADH_ID} from "../app.constants";
import {OrderService} from "./order.service";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  cities: ICityModel[];
  shippers: IShipperModel[];
  order: IOrderModel = {};
  senderCities: ICityModel[];
  citiesTo: ICityModel[] = [];
  showFirstStep: boolean;
  showSecondStep: boolean;
  showConfirmationDetails: boolean;
  showSuccessPage: boolean;
  selectedShipper: IShipperModel;
  readonly store = costOfShippingOnEnum.STORE;
  readonly recipient = costOfShippingOnEnum.RECIPIENT;
  readonly cashOnDelivery = paymentMethodEnum.CASH_ON_DELIVERY;
  readonly prepaid = paymentMethodEnum.PREPAID;
  shippingIcon = faShippingFast;
  downIcon = faAngleDown;
  upIcon = faAngleUp;
  shipperServices: {
    shipper: IShipperModel,
    service: IShipperServiceModel
  }[] = [];
  requiredError: string;
  notSupportingCashOnDelivery: string;
  mustSelectOneError: string;

  constructor(
    protected cityService: CityService,
    protected shipperService: ShipperService,
    protected orderService: OrderService
  ) { }

  ngOnInit() {
    this.order.cityFrom = null;
    this.order.cityTo = null;
    this.showFirstStep = true;
    this.showSecondStep = false;
    this.showConfirmationDetails = false;
    this.showSuccessPage = false;
    this.loadAllData();
  }

  private loadAllData() {
    this.cityService.getAllCity().subscribe(data => {
      if (data.body) {
        this.cities = data.body;
        this.senderCities = data.body.filter((city) => city.canSendFrom);
      }
    }, error => {
      console.log(error);
    });

    this.shipperService.getAll().subscribe(data => {
      if (data.body) {
        this.shippers = data.body;
        this.shippers.forEach(data => {
          data.shipperServiceList.forEach(service => {
            let obj = {
              shipper: data,
              service: service
            };
            this.shipperServices.push(obj);
          })
        })
      }
    }, error => {
      console.log(error)
    });
  }

  cityFromChanged(city: ICityModel) {
    this.notSupportingCashOnDelivery = null;
    this.order.cityTo = null;
    this.citiesTo = city.citiesCanSendTo;
  }

  cityToChanged() {
    this.notSupportingCashOnDelivery = null;
  }

  goNextFromFirstStep() {
    this.requiredError = null;
    this.notSupportingCashOnDelivery = null;
    if (this.validateStepOne()) {
      this.showFirstStep = false;
      this.showSecondStep = true;
    }
  }

  validateStepOne() {
    if (!this.order.cityTo || !this.order.cityFrom
    || !this.order.costOfShippingOn || !this.order.paymentMethod) {
      this.requiredError = 'ERROR';
      return false;
    }
    if ((this.order.cityTo.id !== RIYADH_ID
        || this.order.cityFrom.id !== RIYADH_ID)
        && this.order.paymentMethod === this.cashOnDelivery
    ) {
      this.notSupportingCashOnDelivery = 'ERROR';
      return false;
    }
    return true;
  }

  goNextFromSecondPage() {
    this.mustSelectOneError = null;
    if (this.order.shipperService) {
      this.showSecondStep = false;
      this.showConfirmationDetails = true;
    } else {
      this.mustSelectOneError = 'error';
      window.scroll(0,0);
    }
  }

  submit() {
    if (this.validateStepOne()) {
      if (this.order.cityFrom.citiesCanSendTo.includes(this.order.cityTo)) {
        this.orderService.submitOrder(this.order).subscribe(data => {
          if (data.body) {
            this.order = data.body;
            this.showConfirmationDetails = false;
            this.showSuccessPage = true;
          }
        }, error => {
          console.log(error);
        })
      }
    }
  }

  changePaymentMethod(paymentMethod) {
    this.order.paymentMethod = paymentMethod;
  }

  changeCostOfFeeOn(fee) {
    this.order.costOfShippingOn = fee;
  }

  showServiceDetails(service: IShipperServiceModel) {
    service.showDetails = !service.showDetails;
  }

  pickService(service) {
    this.order.shipperService = service.service;
    this.selectedShipper = service.shipper
  }

}
