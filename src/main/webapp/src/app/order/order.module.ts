import {OrderComponent} from "./order.component";
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
      FontAwesomeModule
    ],
  declarations: [
    OrderComponent
  ],
  entryComponents: [
    OrderComponent
  ],
  exports: [OrderComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class ShyperOrderModule {
  constructor() {
  }
}
