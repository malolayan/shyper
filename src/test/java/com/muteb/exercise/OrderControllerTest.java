package com.muteb.exercise;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.muteb.exercise.Controllers.OrderController;
import com.muteb.exercise.Models.City;
import com.muteb.exercise.Models.Order;
import com.muteb.exercise.Models.ShipperService;
import com.muteb.exercise.Models.enums.CostOfShippingOnEnum;
import com.muteb.exercise.Models.enums.PaymentMethodEnum;
import com.muteb.exercise.Repositories.CityRepository;
import com.muteb.exercise.Repositories.OrderRepository;
import com.muteb.exercise.Services.Mappers.CityMapper;
import com.muteb.exercise.Services.OrderService;
import com.muteb.exercise.Services.dtos.CityDTO;
import com.muteb.exercise.Services.dtos.OrderDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@WebMvcTest(value = OrderController.class)
@SpringBootTest(classes = ExerciseApplication.class)
public class OrderControllerTest {

    Logger logger = LoggerFactory.getLogger(OrderControllerTest.class);

    private static final ObjectMapper mapper = createObjectMapper();

    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private CityMapper cityMapper;

    @Autowired
    private CityRepository cityRepository;



    private MockMvc mockOrder;

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(
            MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), StandardCharsets.UTF_8);


    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        final OrderController orderController = new OrderController(orderService, orderRepository, cityMapper, cityRepository);
        this.mockOrder = MockMvcBuilders.standaloneSetup(orderController).build();
    }

    public OrderDTO createOrder() {
        City city = cityRepository.getOne(1L);
        OrderDTO order = new OrderDTO();
        order.setCityFrom(cityMapper.toDto(city));
        order.setCityTo(cityMapper.toDto(city));
        order.setCostOfShippingOn(CostOfShippingOnEnum.STORE);
        order.setPaymentMethod(PaymentMethodEnum.PREPAID);
        order.setShipperService(new ShipperService(1L));
        return order;
    }

    @Test
    @Transactional
    public void submitOrder() throws Exception {
        logger.debug("###### TESTING SUBMITTING VALID ORDER STARTED ######");
        OrderDTO order = createOrder();
        int currentDatabaseSize = orderRepository.findAll().size();
        mockOrder.perform(post("/api/order")
        .contentType(APPLICATION_JSON_UTF8)
        .content(convertObjectToJsonBytes(order)))
                .andExpect(status().isCreated());

        List<Order> orderList = orderRepository.findAll();
        assertThat(orderList).hasSize(currentDatabaseSize + 1);
        Order latestAddedOrder = orderList.get(orderList.size() - 1);

        assertThat(latestAddedOrder.getCityFrom().getId().equals(order.getCityFrom().getId()));
        assertThat(latestAddedOrder.getCityTo().getId().equals(order.getCityTo().getId()));
        assertThat(latestAddedOrder.getCostOfShippingOn().name().equals(order.getCostOfShippingOn().name()));
        assertThat(latestAddedOrder.getShipperService().getId().equals(order.getShipperService().getId()));

        logger.debug("###### TESTING SUBMITTING VALID ORDER ENDED ######");
    }

    // should return bad request
    @Test
    @Transactional
    public void addOrderWithExistingId() throws Exception {
        logger.debug("###### TESTING SUBMITTING INVALID ORDER STARTED ######");
        int currentDatabaseSize = orderRepository.findAll().size();
        OrderDTO order = createOrder();
        order.setId(1L);
        mockOrder.perform(post("/api/order")
                .contentType(APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(order)))
                .andExpect(status().isBadRequest());

        List<Order> orders = orderRepository.findAll();
        assertThat(orders).hasSize(currentDatabaseSize);

        logger.debug("###### TESTING SUBMITTING INVALID ORDER ENDED ######");
    }

    public static byte[] convertObjectToJsonBytes(Object object)
            throws IOException {
        return mapper.writeValueAsBytes(object);
    }

    private static ObjectMapper createObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }
}
